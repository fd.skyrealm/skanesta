FROM python:3.8
WORKDIR /app/
COPY . .
# for security reasons
RUN adduser -D -g '' user
USER user
RUN chown -R user:user /app/*

CMD ["python", "/app/kafka.py"]