import time
from generator import generate_data
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers='localhost:9092')

#send data to Kafka Producer
while True:
    data = generate_data()
    producer.send('data', data)
    time.sleep(0.001)
