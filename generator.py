#/usr/bin/python3
#create generator of data { bid_01: float, bid_02: float, bid_03: float}

import time
import random
import json

def generate_data(bids = 50): 
  data = {}
  # Generate timestamp in Unix format
  data["timestamp"] = int(time.time())
  # Generate bid and ask values
  data["bid"] = {}
  data["ask"] = {}
  for i in range(1, bids+1):
    data["bid"][f"bid_{i:02d}"] = random.uniform(1, i*10)
    data["ask"][f"ask_{i:02d}"] = random.uniform(1, i*10)
  # Generate stats object
  data["stats"] = {}
  data["stats"]["bid_middle"] = sum(data["bid"].values()) / len(data["bid"])
  data["stats"]["ask_middle"] = sum(data["ask"].values()) / len(data["ask"])
  return data


if __name__ == "__main__":
    print(json.dumps(generate_data()))